package de.pi41.utils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;

public class FileIO {
	
	public static String readFile(String filename) {
		String content = "";
	    try (BufferedReader br = new BufferedReader(new java.io.FileReader(
	    		filename))) {
	        StringBuilder sb = new StringBuilder();
	        String line = br.readLine();

	        while (line != null) {
	            sb.append(line);
	            sb.append(System.lineSeparator());
	            line = br.readLine();
	        }
	        content = sb.toString();
	    } catch (IOException e) {
			e.printStackTrace();
		}
		return content;
	}
	
	public static void writeFile(String content, String filename) {
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(
		          new FileOutputStream(filename), "utf-8"))) {
		    writer.write(content);
		    writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
