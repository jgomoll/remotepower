package de.pi41.funk;

import java.io.IOException;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

import de.pi41.utils.FileIO;
import de.pi41.utils.TerminalUtils;

@Path("/")
public class RemotePowerControl {
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String getLandingPage() {
		return FileIO.readFile("webapps/funk/index.html");
	}
	
	@POST
	@Path("/send")
	public void sendCommandThroughForm(@FormParam("housecode") String housecode,
			@FormParam("outlet") String outlet,
			@FormParam("state") String state,
			@Context HttpServletResponse servletResponse) {
		String command = "sudo send433 " + housecode + " " + outlet
				+ " " + state;
		TerminalUtils.doTerminalCall(command);
		try {
			servletResponse.sendRedirect("../funk");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@GET
	@Path("{housecode}/{outlet}/{state}")
	@Produces(MediaType.TEXT_HTML)
	public String sendCommandThroughPath(@PathParam("housecode") String housecode,
			@PathParam("outlet") String outlet,
			@PathParam("state") String state) {
		String command = "sudo send433 " + housecode + " " + outlet
				+ " " + state;
		TerminalUtils.doTerminalCall(command);
		return "<html><body><h1>" + command + "</h1></body></html>";
	}

}
